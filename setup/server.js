import express from 'express';

export const serverApp = express();

serverApp.listen(process.env.SERVER_PORT, process.env.SERVER_HOST);