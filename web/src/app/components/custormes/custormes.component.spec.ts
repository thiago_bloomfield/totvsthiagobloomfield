import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustormesComponent } from './custormes.component';

describe('CustormesComponent', () => {
  let component: CustormesComponent;
  let fixture: ComponentFixture<CustormesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustormesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustormesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
