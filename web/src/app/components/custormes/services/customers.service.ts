import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from './../../../../environments/environment';

@Injectable()
export class CustomersService {

  constructor(
    private http: HttpClient
  ) { }

  getAll(): Observable<any> {
    return this.http.get('/api/v1/customers');
  }

  getOverdueCustomers(): Observable<any> {
    return this.http.get('/api/v1/overdue_customers');
  }

}
