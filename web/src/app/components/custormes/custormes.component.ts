import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { CustomersService } from './services/customers.service';

@Component({
  selector: 'app-custormes',
  templateUrl: './custormes.component.html',
  styleUrls: ['./custormes.component.scss']
})
export class CustormesComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = ['nome', 'valor', 'desde'];
  dataSource: MatTableDataSource<any>;
  resultsLength = 0;

  constructor (
    private customersService: CustomersService
  ) { }

  ngOnInit(): void {
    this.customersService
      .getOverdueCustomers()
      // .getAll()
      .subscribe(data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
