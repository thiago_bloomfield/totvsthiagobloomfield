import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CustormesComponent } from './components/custormes/custormes.component';


const routes: Routes = [
  {
    path: 'customers',
    component: CustormesComponent
  },
  {
    path: 'overdue_customers',
    component: CustormesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
