import mongoose from 'mongoose';
import { CustomersData } from './customers.data';

const schema = new mongoose.Schema(
  {
    nome: String,
    valor: String,
    desde: Date
  }
);

mongoose.connection.once('open', () => {
  const CustomersModel = mongoose.model('Customers', schema);
  let overdue = false;

  CustomersData.forEach((customer, index) => {
    const customerToAdd = new CustomersModel(customer);
    customerToAdd.save((error, customerInserted) => {
      if (error) {
        console.log(error);
      } else {
        console.log(`Cliente ${customerInserted.nome} inserído com sucesso!`);
      }
    });
  });

});

export const CustomersModel = mongoose.model('Customers', schema);
