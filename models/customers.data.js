export const CustomersData = [
    {
        nome: 'Cliente 1',
        valor: '150000.00',
        desde: new Date('2020', '03', '01')
    },
    {
        nome: 'Cliente 2',
        valor: '50000.00',
        desde: new Date('2020', '04', '01')
    },
    {
        nome: 'Cliente 3',
        valor: '10000.00',
        desde: new Date('2020', '05', '01')
    },
    {
        nome: 'Cliente 4',
        valor: '3000.00',
        desde: new Date('2020', '06', '01')
    },
    {
        nome: 'Cliente 5',
        valor: '250.00',
        desde: new Date('2020', '07', '01')
    },
    {
        nome: 'Cliente 6',
        valor: '323.00',
        desde: new Date('2020', '08', '01')
    },
    {
        nome: 'Cliente 7',
        valor: '145.00',
        desde: new Date('2020', '09', '01')
    }
];
