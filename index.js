import './setup/db';
import { serverApp } from './setup/server';

import express from 'express';

import { CustomersModel } from './models/customers';

const router = express.Router();

serverApp.get('/api/v1/customers', async (req, res) => {
    try {
        const customers = await CustomersModel.find();
        res
            .status(200)
            .json(customers);
    } catch (error) {
        res
            .status(500)
            .send(error)
    }
});

serverApp.get('/api/v1/overdue_customers', async (req, res) => {
    try {
        const today = new Date();
        const customers = await CustomersModel
            .find(
                {
                    'desde': { "$gte": new Date(2012, 7, 14), '$lte': today }
                }
            );
        res
            .status(200)
            .json(customers);
    } catch (error) {
        res
            .status(500)
            .send(error);
    }
});

serverApp.get('/', (req, res) => {
    res.send('Bem vindo!');
});


