Docker:
 - Node
 - MongoDB
Local
 - Angular 9

Primeiro, certifique-se de ter o MinGW instalado. (Vide obs abaixo).
Para rodar o backend, executar na pasta raiz o comando "make up", vai subir os dois containers
Para rodar o fron end, vá na pasta web e rode o comando "npm install -g @angular/cli", vai instalar o cli do angular, depois rode o comando "npm install" vai instalar todas as dependencias do sistema, por fim rode o comando "ng s" para inicializar o sistema, após concluído, abra o navegador e cole a url "http://localhost:4200/customers" e aparecerá a tela do teste.

OBS: Necessita do comando make instalado pois criei com variaveis na .env

Obrigado.